// ordenamientosG13.cpp: archivo de proyecto principal.

#include "stdafx.h"
#include<iostream>

using namespace System;
using namespace std;

struct nodo{
	int nro;
	struct nodo *sgte, *ant;
};
typedef struct nodo *Tlista;

Tlista cabecera;

void menu();
void Mostrar(Tlista lista);
void Insertar_Final(Tlista &lista);
void Burbuja(Tlista &lista);
void Intercambio(Tlista &lista);
void Ord_Insercion(Tlista &lista);
void Seleccion(Tlista &lista);
void Quicksort(Tlista &lista, int inicial, int final);
void Ordenamiento_Shell(Tlista &lista, int cont);
void MergeSort(Tlista &lista);
void Merge(Tlista cab, Tlista &prim, Tlista &fin);
Tlista comparacion(Tlista a, Tlista b);
void radixsort(Tlista &lista, Tlista vector[], int tam);
void InsertarFinalRadix(Tlista &lista, int nro);
void radixdelete(Tlista vector[]);
void radixmove(Tlista &lista, Tlista vector[]);

int main(){
	menu();
	return 0;
}

void menu(){
	Tlista lista = NULL;
	Tlista vector = NULL;
	int opcion = 0, n = 0, cont = 0;
	system("cls");

	cout << endl;
	do{
		cout << "\t\t\t浜様様様様様様様様様様様様様様様様様融" << endl;
		cout << "\t\t\t�                MENU                �" << endl;
		cout << "\t\t\t麺様様様様様様様様様様様様様様様様様郵" << endl;
		cout << "\t\t\t�  1. Insertar numero                �" << endl;
		cout << "\t\t\t�  2. Ordenar por burbuja            �" << endl;
		cout << "\t\t\t�  3. Ordenar por seleccion          �" << endl;
		cout << "\t\t\t�  4. Ordenar por insercion          �" << endl;
		cout << "\t\t\t�  5. Ordenar por intercambio        �" << endl;
		cout << "\t\t\t�  6. Ordenar por quicksort          �" << endl;
		cout << "\t\t\t�  7. Ordenar por shell              �" << endl;
		cout << "\t\t\t�  8. Ordenar por mergeSort          �" << endl;
		cout << "\t\t\t�  9. Ordenar por RadixSort          �" << endl;
		cout << "\t\t\t�  10. Mostrar elementos             �" << endl;
		cout << "\t\t\t�  11. Salir                         �" << endl;
		cout << "\t\t\t藩様様様様様様様様様様様様様様様様様夕" << endl;
		cout << "\t\t\t\tIngrese una opcion: ";

		cin >> opcion;

		switch (opcion){

		case 1:
			Insertar_Final(lista);
			cont++;
			system("cls");
			break;

		case 2:
			Burbuja(lista);
			system("pause");
			system("cls");
			break;

		case 3:
			Seleccion(lista);
			system("pause");
			system("cls");
			break;

		case 4:
			cout << "PARA UNA LISTA VACIA" << endl;
			Ord_Insercion(lista);
			system("pause");
			system("cls");
			break;

		case 5:
			Intercambio(lista);
			system("pause");
			system("cls");
			break;

		case 6:
			Quicksort(lista, 1, cont);
			cout << endl << "Se ha ordenado por el metodo de QUICKSORT" << endl << endl;
			system("pause");
			system("cls");
			break;

		case 7:
			Ordenamiento_Shell(lista, cont);
			system("PAUSE");
			system("cls");
			break;

		case 8:
			MergeSort(lista);
			cout << endl << "Se ha ordenado por el metodo de MERGESORT" << endl << endl;
			system("PAUSE");
			system("cls");
			break;

		case 9:
			radixsort(lista, &vector, cont);
			cout << endl << "Se ha ordenado por el metodo de RADIXSORT" << endl << endl;
			system("PAUSE");
			system("cls");
			break;

		case 10: Mostrar(lista);
			system("pause");
			system("cls");
			break;

		case 11:	system("exit");
			break;
		}
	} while (opcion != 11);
}


void Insertar_Final(Tlista &lista){
	int n;
	Tlista nuevo = NULL;
	Tlista q = lista;
	nuevo = new(struct nodo);
	cout << "Ingrese el valor del nodo: ";
	cin >> n;
	nuevo->nro = n;
	if (lista == NULL){
		nuevo->sgte = NULL;
		lista = nuevo;
	}
	else{
		while (q->sgte != NULL){
			q = q->sgte;
		}
		nuevo->sgte = NULL;
		q->sgte = nuevo;
		nuevo->ant = q;
	}
}



void Mostrar(Tlista lista){
	Tlista q = lista;
	while (q->sgte != NULL){
		cout << q->nro << "  ";
		q = q->sgte;
	}
	cout << q->nro << "  ";
	cout << endl;
}


void Burbuja(Tlista &lista){

	Tlista p = lista;
	Tlista q = NULL;
	int n;
	while (lista->sgte != q){
		while (p->sgte != q){
			if (p->nro > p->sgte->nro){
				n = p->nro;
				p->nro = p->sgte->nro;
				p->sgte->nro = n;
			}
			p = p->sgte;
		}
		q = p;
		p = lista;
	}
	cout << endl << "Se ha ordenado por el metodo de BURBUJA" << endl << endl;
}

void Intercambio(Tlista &lista){
	Tlista q = lista, p = lista;
	int aux;
	while (p->sgte != NULL){

		while (q->sgte != NULL){
			q = q->sgte;
			if (p->nro >= q->nro){
				aux = p->nro;
				p->nro = q->nro;
				q->nro = aux;
			}

		}
		p = p->sgte;
		if (q->sgte == NULL){
			while (q != p){
				q = q->ant;


			}
		}

	}
	cout << endl << "Se ha ordenado por el metodo de INTERCAMBIO" << endl << endl;
}


void Ord_Insercion(Tlista &lista){
	int num;
	int elem;
	cout << "Ingrese el valor nuevo:  ";
	cin >> elem;
	Tlista nuevo, inicio = lista;
	nuevo = new(struct nodo);
	nuevo->nro = elem;
	nuevo->sgte = NULL;

	if (lista == NULL)
		lista = nuevo;
	else{
		Tlista aux = lista, fin = NULL;
		while (aux->sgte != NULL)
			aux = aux->sgte;
		aux->sgte = nuevo;
		fin = nuevo;
		while (inicio->sgte != NULL){
			if (fin->nro<inicio->nro){
				num = fin->nro;
				fin->nro = inicio->nro;
				inicio->nro = num;
			}
			inicio = inicio->sgte;
		}
	}

}




void Seleccion(Tlista &lista){
	int n = 0;

	Tlista p = lista, q = lista, t = lista;
	while (p->sgte != NULL){
		int	m = 9999999;
		q = p->sgte;
		t = p;

		if (q->sgte != NULL){
			while (q->sgte != NULL)
			{
				if (m>q->nro){
					m = q->nro;
				}
				q = q->sgte;

			}
			if (m>q->nro){
				m = q->nro;
			}
		}
		else{
			if (m>p->nro)
			{
				m = p->nro;
			}
			if (m>q->nro)
			{
				m = q->nro;
			}

		}

		while (t->nro != m){
			t = t->sgte;
		}

		if (m<p->nro){
			n = p->nro;
			p->nro = m;
			t->nro = n;

		}
		p = p->sgte;

	}
	cout << endl << "Se ha ordenado por el metodo de SELECCION" << endl << endl;
}

void Quicksort(Tlista &lista, int inicial, int final){
	int i, j, pivot;
	Tlista p = lista, q = lista, r = lista;

	for (int x = 1; x<((inicial + final) / 2); x++){
		r = r->sgte;
	}

	pivot = r->nro;

	i = inicial; j = final;

	for (int y = 1; y<inicial; y++){
		p = p->sgte;
	}
	for (int z = 1; z<final; z++){
		q = q->sgte;
	}

	do{
		while (p->nro<pivot){
			p = p->sgte;
			i++;
		}
		while (q->nro>pivot){
			q = q->ant;
			j--;
		}
		if (i <= j){
			int aux;
			aux = p->nro;
			p->nro = q->nro;
			q->nro = aux;
			i++; p = p->sgte;
			j--; q = q->ant;
		}
	} while (i <= j);

	if (inicial<j){
		Quicksort(lista, inicial, j);
	}
	if (i<final){
		Quicksort(lista, i, final);
	}

}




void Ordenamiento_Shell(Tlista &lista, int cont){
	int aux = 0, n = cont;
	Tlista q = lista;
	Tlista r = lista;
	while (n != 0){
		for (int i = 0; i<n / 2; i++){
			r = r->sgte;
		}
		while (r != NULL){

			if (r->nro<q->nro){
				aux = q->nro;
				q->nro = r->nro;
				r->nro = aux;
			}
			q = q->sgte;
			r = r->sgte;
		}

		if (r == NULL){
			q = lista;
			r = lista;

		}

		n = n / 2;

	}
	r = r->sgte;
	while (r != NULL){
		if (r->nro<q->nro){
			aux = q->nro;
			q->nro = r->nro;
			r->nro = aux;
		}
		q = q->sgte;
		r = r->sgte;
	}
	cout << endl << "Se ha ordenado por el metodo de SHELL" << endl << endl;
}

void MergeSort(Tlista &lista){
	Tlista cab = lista;
	Tlista p = NULL;
	Tlista q = NULL;

	if (cab == NULL || cab->sgte == NULL){
		return;
	}
	else{

		Merge(cab, p, q);
		MergeSort(p);
		MergeSort(q);
		lista = comparacion(p, q);
	}
}

void Merge(Tlista cab, Tlista &i, Tlista &f){
	Tlista s;
	Tlista t;

	if (cab == NULL || cab->sgte == NULL){
		i = cab;
		f = NULL;
	}
	else{
		t = cab;
		s = cab->sgte;

		while (s != NULL){
			s = s->sgte;
			if (s != NULL){
				t = t->sgte;
				s = s->sgte;
			}
		}
		i = cab;
		f = t->sgte;
		t->sgte = NULL;
	}
}

Tlista comparacion(Tlista p, Tlista q){
	Tlista r = NULL;

	if (p == NULL){
		return q;
	}
	else{
		if (q == NULL){
			return p;
		}
	}

	if (p->nro <= q->nro){
		r = p;
		r->sgte = comparacion(p->sgte, q);
	}
	else{
		r = q;
		r->sgte = comparacion(p, q->sgte);
	}

	return r;
}

void radixsort(Tlista &lista, Tlista vector[], int tam){

	if (tam >= 2){
		Tlista p = NULL;
		int part = 10, div = 1, cifra;
		bool interruptor;
		do{
			p = lista;
			interruptor = false;
			while (p != NULL)
			{
				cifra = p->nro / div;
				if (cifra != 0){
					interruptor = true;
				}
				cifra = cifra%part;
				InsertarFinalRadix(vector[cifra], p->nro);
				p = p->sgte;
			}
			radixmove(lista, vector);
			radixdelete(vector);
			div = div * 10;
		} while (interruptor);
	}
}

void radixmove(Tlista &lista, Tlista vector[]){
	for (int i = 0; i < 10; i++){
		while (vector[i] != NULL){
			lista->nro = vector[i]->nro;
			lista = lista->sgte;
			vector[i] = vector[i]->sgte;
		}
	}
}

void radixdelete(Tlista vector[]){
	Tlista p = NULL;
	for (int i = 0; i < 10; i++){
		while (vector[i] != NULL)
		{
			p = vector[i];
			vector[i] = vector[i]->sgte;
			delete(p);
		}
	}
}

void InsertarFinalRadix(Tlista &lista, int nro){
	Tlista nuevo = NULL;
	Tlista q = lista;
	nuevo = new(struct nodo);
	nuevo->nro = nro;
	if (lista == NULL){
		nuevo->sgte = NULL;
		lista = nuevo;
	}
	else{
		while (q->sgte != NULL){
			q = q->sgte;
		}
		nuevo->sgte = NULL;
		q->sgte = nuevo;
		nuevo->ant = q;
	}
}



